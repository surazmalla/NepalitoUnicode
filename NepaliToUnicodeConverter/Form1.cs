﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NepaliToUnicodeConverter
{
    public partial class Form1 : Form
    {
        int InsertBatchSize = 1000; // YOU CAN INCREASE AS YOUR NEED

        DataTable DTCitizenMaster = new DataTable("CitizenMaster_NEW");
        string ConnectionString = "Data Source='suraj';initial catalog='font'; User Id=sa; Password=sandman;";
        
        public Form1()
        {
            InitializeComponent();
        }

        void intiCitizenMaster()
        {
            DTCitizenMaster = new DataTable("CitizenMaster_NEW");

            DTCitizenMaster.Columns.Add("SN", typeof(Int32));
            DTCitizenMaster.Columns.Add("FNepName", typeof(string));
            DTCitizenMaster.Columns.Add("LNepName", typeof(string));
        }


        private void Form1_Load(object sender, EventArgs e)
        {
        }

        private void btnConvert_Click(object sender, EventArgs e)
        {
            intiCitizenMaster();
            txtUnicode.Text = txtUnicode.Text = string.Empty;

            if (cbFontList.SelectedIndex == -1)
            {
                MessageBox.Show("Please select original font.", "Nepali to Unicode Converter");
                return;
            }

            if (txtNepali.Text.Trim().Length == 0)
            {
                MessageBox.Show("The original text should not empty.", "Nepali to Unicode Converter");
                return;
            }

            if (cbFontList.SelectedIndex == 0)
            {
                KantipurToUnicode aa = new KantipurToUnicode();
                txtUnicode.Text = aa.kantipurConfigToUnicode(txtNepali.Text);
            }

            else if (cbFontList.SelectedIndex == 1)
            {
                PreetiToUnicode aa = new PreetiToUnicode();
                txtUnicode.Text = aa.convert_to_unicode(txtNepali.Text);
            }

            else if (cbFontList.SelectedIndex == 2)
            {
                ShangrilaNumericToUnicode aa = new ShangrilaNumericToUnicode();
                txtUnicode.Text = aa.ShangrilaConfigToUnicode(txtNepali.Text);

                //return;
                string ExcuteQuery = "Select SN,FNepName, LNepName  from CitizenMaster";
                SqlConnection conn = new SqlConnection(ConnectionString);
                SqlCommand cmd = new SqlCommand(ExcuteQuery, conn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    string FirstName = aa.ShangrilaConfigToUnicode(dt.Rows[i]["FNepName"].ToString());
                    string LastName = aa.ShangrilaConfigToUnicode(dt.Rows[i]["LNepName"].ToString());

                    DataRow drowText = DTCitizenMaster.NewRow();
                    drowText["SN"] = dt.Rows[i]["SN"].ToString();
                    drowText["FNepName"] = FirstName;
                    drowText["LNepName"] = LastName;
                    DTCitizenMaster.Rows.Add(drowText);


                    if (DTCitizenMaster.Rows.Count >= InsertBatchSize)
                        BulkInsert();
                }

                if (DTCitizenMaster.Rows.Count>0) //CHECK BEFORE EXIT
                    BulkInsert();

                MessageBox.Show("Completed");
            }


        }

        void BulkInsert()
        {
            using (SqlConnection dbConnection = new SqlConnection(ConnectionString))
            {
                dbConnection.Open();
                using (SqlBulkCopy s = new SqlBulkCopy(dbConnection))
                {
                    s.DestinationTableName = DTCitizenMaster.TableName;

                    foreach (var column in DTCitizenMaster.Columns)
                        s.ColumnMappings.Add(column.ToString(), column.ToString());

                    s.WriteToServer(DTCitizenMaster);
                }
            }

            intiCitizenMaster();
        }

        private void DoBulkUpdate()
        {
            string CreateTempTabe = @"
                                      CREATE TABLE #TEMP 
                                        (
                                            SN int,
                                            FNepName NVARCHAR(max),
                                            LNepName NVARCHAR(max)
                                        )";

            string UpdateQuery = @"UPDATE 
	                                    CitizenMaster 
                                    SET 
                                        SN=TP.SN,
                                        FNepName=TP.FNepName,
                                        LNepName=TP.LNepName
                                    FROM 
	                                    CitizenMaster T1,#temp Tp
                                    WHERE
	                                    T1.SN=TP.SN; 
                                DROP TABLE #temp;";

            using (SqlConnection LocalConnection = new SqlConnection(ConnectionString))
            {
                using (SqlCommand command = new SqlCommand("", LocalConnection))
                {
                    LocalConnection.Open();
                    command.CommandText = CreateTempTabe;
                    command.ExecuteNonQuery();


                    using (SqlBulkCopy bulkcopy = new SqlBulkCopy(LocalConnection))
                    {
                        bulkcopy.BulkCopyTimeout = 300;
                        bulkcopy.DestinationTableName = "#temp";
                        bulkcopy.WriteToServer(DTCitizenMaster);
                        bulkcopy.Close();
                    }

                    command.CommandTimeout = 300;
                    command.CommandText = UpdateQuery;
                    command.ExecuteNonQuery();

                }

            }

            intiCitizenMaster();
        }
    }
}
